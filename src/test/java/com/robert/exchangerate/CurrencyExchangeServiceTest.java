package com.robert.exchangerate;

import com.robert.exchangerate.data.CurrencyExchange;
import com.robert.exchangerate.data.CurrencyExchangeOperations;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;


import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ContextConfiguration(classes = ExchangeRateApplication.class)
@DataJpaTest
public class CurrencyExchangeServiceTest {
    
    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private CurrencyExchangeOperations currencyExchangeOperations;

    @Test
    public void testFindByCurrency() {
        Double rate = 1.0;
        testEntityManager.persist(new CurrencyExchange("EUR", rate, null, LocalDate.now()));

        CurrencyExchange currencyExchange = currencyExchangeOperations.findByCurrency("EUR");
        assertEquals(rate, currencyExchange.getRate());
    }
}
