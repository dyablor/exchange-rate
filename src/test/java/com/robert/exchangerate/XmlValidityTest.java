package com.robert.exchangerate;

import com.robert.exchangerate.data.xml.CurrencyRate;
import com.robert.exchangerate.data.xml.DataSet;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;


import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class XmlValidityTest {

    private DataSet dataSet;

    @BeforeAll
    public void setUp() throws JAXBException {
        File xmlCurrencyExchange = new File("src/test/resources/nbrfxrates.xml");
        JAXBContext jaxbContext = JAXBContext.newInstance(DataSet.class);

        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        dataSet = (DataSet) jaxbUnmarshaller.unmarshal(xmlCurrencyExchange);
    }
    
    @Test
    public void checkRates() {
        List<CurrencyRate> currencyRateList = dataSet.getBody().getCube().getCurrencyRates();
        double eurRate = 4.8747;
        double usdRate = 4.1588;
        CurrencyRate eur = currencyRateList.stream().filter(currencyRate -> currencyRate.getCurrency().equals("EUR")).findFirst().orElse(null);
        CurrencyRate usd = currencyRateList.stream().filter(currencyRate -> currencyRate.getCurrency().equals("USD")).findFirst().orElse(null);
        assertNotNull(eur);
        assertNotNull(usd);
        
        assertEquals(eur.getRate(), eurRate);
        assertEquals(usd.getRate(), usdRate);
    }
}
