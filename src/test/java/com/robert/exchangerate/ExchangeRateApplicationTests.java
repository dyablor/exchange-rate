package com.robert.exchangerate;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = ExchangeRateApplication.class)
class ExchangeRateApplicationTests {

	@Test
	void contextLoads() {
	}

}
