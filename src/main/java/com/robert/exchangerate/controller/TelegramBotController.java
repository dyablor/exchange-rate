package com.robert.exchangerate.controller;

import com.robert.exchangerate.data.CurrencyExchangeInfo;
import com.robert.exchangerate.service.CurrencyExchangeService;
import com.w4p.telegram.annotation.W4TelegramBot;
import com.w4p.telegram.annotation.W4TelegramCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.User;

@W4TelegramBot
public class TelegramBotController {

    @Autowired
    private CurrencyExchangeService currencyExchangeService;

    @W4TelegramCommand(value = "/test", description = "Test method with user input")
    public SendMessage test(User user, String argument) {
        return new SendMessage()
                .setChatId(user.getId().longValue())
                .setText("Hi " + user.getFirstName() + "! You sent the following message: \"" + argument + "\"");
    }

    @W4TelegramCommand(value = "/exchange", description = "Usage: [amount] [fromCurrency] [toCurrency]")
    public SendMessage exchange(User user, String arguments) {
        return new SendMessage()
                .setChatId(user.getId().longValue())
                .setText(calculateExchangeRate(user, arguments));
    }

    private String calculateExchangeRate(User user, String userCommand) {
        String[] split = userCommand.split("\\s+");
        String fromCurrency = split[1];
        String toCurrency = split[2];
        String amount = split[0];

        CurrencyExchangeInfo currencyExchangeInfo = currencyExchangeService.performExchange(fromCurrency, toCurrency, Double.parseDouble(amount));

        return "The exchange rate for: " + amount + " " + fromCurrency + " = " + currencyExchangeInfo.getRoundedParity() + " " + toCurrency;
    }
}
