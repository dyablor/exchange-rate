package com.robert.exchangerate.controller;

import com.robert.exchangerate.data.CurrencyExchange;
import com.robert.exchangerate.data.CurrencyExchangeInfo;
import com.robert.exchangerate.service.CurrencyExchangeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/exchange-rate")
@SuppressWarnings({ "rawtypes", "unchecked" })
public class CurrencyExchangeController {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CurrencyExchangeService currencyExchangeService;

    @RequestMapping(value = "/{fromCurrency}/", method = RequestMethod.GET)
    public ResponseEntity<CurrencyExchangeInfo> exchangeCurrency(
            @RequestParam(name = "transform", defaultValue = "RON") String toCurrency,
            @PathVariable("fromCurrency") String fromCurrency,
            @RequestParam(name = "amount", defaultValue = "1.00") Double amount) {
        
        log.debug("Convert: from [{}], to [{}], amount: [{}]", fromCurrency, toCurrency, amount);

        CurrencyExchangeInfo currencyExchangeInfo = currencyExchangeService.performExchange(fromCurrency, toCurrency, amount);

        if (currencyExchangeInfo == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resources: " + fromCurrency + ", " + toCurrency);
        }
        
        return new ResponseEntity<>(currencyExchangeInfo, HttpStatus.OK);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<CurrencyExchange>> listAllRates() {
        List<CurrencyExchange> allCurrencies = currencyExchangeService.getAllRates();
  
        if(allCurrencies == null || allCurrencies.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resources");
        }
                
        return new ResponseEntity<>(allCurrencies, HttpStatus.OK);
    }
}
