package com.robert.exchangerate;

import com.w4p.telegram.annotation.EnableW4TelegramBot;
import com.w4p.telegram.config.TelegramBotBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableW4TelegramBot
public class ExchangeRateBot extends SpringBootServletInitializer {

    @Value("${bot.username}")
    private String botUser;
    @Value("${bot.token}")
    private String botToken;

    @Bean
    public TelegramBotBuilder telegramBotBuilder() {
        return new TelegramBotBuilder()
                .username(botUser)
                .token(botToken);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ExchangeRateBot.class);
    }


    public static void main(String[] args) {
        SpringApplication.run(ExchangeRateBot.class, args);
    }
}

