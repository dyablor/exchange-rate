package com.robert.exchangerate.data;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CurrencyExchangeOperations extends CrudRepository<CurrencyExchange, Long> {
    CurrencyExchange findByCurrency(String currency);

    List<CurrencyExchange> findAll();
    
    void delete(CurrencyExchange currencyExchange);
}
