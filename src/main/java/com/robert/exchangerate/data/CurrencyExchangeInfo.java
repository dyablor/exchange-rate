package com.robert.exchangerate.data;

import java.time.LocalDate;

public class CurrencyExchangeInfo {
    private final String firstCurrency;
    private final Double parity;
    private final String secondCurrency;
    private final String roundedParity;
    private final LocalDate date;
    
    public CurrencyExchangeInfo(String firstCurrency, Double parity, String secondCurrency, String roundedParity, LocalDate date) {
        this.firstCurrency = firstCurrency;
        this.secondCurrency = secondCurrency;
        this.parity = parity;
        this.roundedParity = roundedParity;
        this.date = date;
    }
    
    public String getFirstCurrency() {
        return firstCurrency;
    }

    public String getSecondCurrency() {
        return secondCurrency;
    }

    public Double getParity() {
        return parity;
    }

    public String getRoundedParity() {
        return roundedParity;
    }

    public LocalDate getDate() {
        return date;
    }
}
