package com.robert.exchangerate.data.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LT_Header", namespace = "http://www.bnr.ro/xsd", propOrder = {
        "publisher",
        "publishingDate",
        "messageType"
})
public class Header {

    @XmlElement(name = "Publisher", namespace = "http://www.bnr.ro/xsd", required = true)
    protected String publisher;

    @XmlElement(name = "PublishingDate", namespace = "http://www.bnr.ro/xsd", required = true)
    @XmlSchemaType(name = "date")
    protected String publishingDate;

    @XmlElement(name = "MessageType", namespace = "http://www.bnr.ro/xsd", required = true)
    protected String messageType;

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getPublishingDate() {
        return publishingDate;
    }

    public void setPublishingDate(String publishingDate) {
        this.publishingDate = publishingDate;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }
}