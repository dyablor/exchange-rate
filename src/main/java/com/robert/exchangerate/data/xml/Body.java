package com.robert.exchangerate.data.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "subject",
        "description",
        "origCurrency",
        "cubes"
})
public class Body {
    @XmlElement(name = "Subject", namespace = "http://www.bnr.ro/xsd", required = true)
    protected String subject;
    
    @XmlElement(name = "Description", namespace = "http://www.bnr.ro/xsd")
    protected String description;
    
    @XmlElement(name = "OrigCurrency", namespace = "http://www.bnr.ro/xsd", required = true)
    protected String origCurrency;
    
    @XmlElement(name = "Cube", namespace = "http://www.bnr.ro/xsd", required = true)
    protected List<CubeElement> cubes;
    
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrigCurrency() {
        return origCurrency;
    }

    public void setOrigCurrency(String origCurrency) {
        this.origCurrency = origCurrency;
    }

     public CubeElement getCube() {
        if (cubes == null) {
            cubes = new ArrayList<>();
        }
        return this.cubes.get(0);
    }
}
