package com.robert.exchangerate.data.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "header",
        "body"
})
@XmlRootElement(name = "DataSet", namespace = "http://www.bnr.ro/xsd")
public class DataSet {

    @XmlElement(name = "Header", namespace = "http://www.bnr.ro/xsd", required = true)
    protected Header header;
    
    @XmlElement(name = "Body", namespace = "http://www.bnr.ro/xsd", required = true)
    protected Body body;

    public Header getHeader() {
        return header;
    }
    
    public void setHeader(Header header) {
        this.header = header;
    }
    
    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }
}
