package com.robert.exchangerate.data.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Cube", namespace = "http://www.bnr.ro/xsd", propOrder = {
        "currencyRates"
})
public class CubeElement {

    @XmlElement(name = "Rate", namespace = "http://www.bnr.ro/xsd", required = true)
    private List<CurrencyRate> currencyRates;

    @XmlAttribute(name = "date", required = true)
    @XmlSchemaType(name = "date")
    private String date;

    public List<CurrencyRate> getCurrencyRates() {
        if (currencyRates == null) {
            currencyRates = new ArrayList<>();
        }
        return this.currencyRates;
    }
    
    public String getDate() {
        return date;
    }
    
    public void setDate (String date) { 
        this.date = date;
    }
}
