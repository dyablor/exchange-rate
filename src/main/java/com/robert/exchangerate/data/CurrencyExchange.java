package com.robert.exchangerate.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.Nullable;

import javax.persistence.*;
import java.time.LocalDate;

@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "currency", "date" }))
@Entity
public class CurrencyExchange {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    @JsonIgnore
    private Long id;

    @Column(nullable = false)
    private String currency;

    @Column(nullable = false)
    private Double rate;

    @Column
    @Nullable
    private Integer multiplier;

    @Column(nullable = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    public CurrencyExchange(String currency, Double rate, Integer multiplier, LocalDate date) {
        this.currency = currency;
        this.rate = rate;
        this.multiplier = multiplier;
        this.date = date;
    }

    public CurrencyExchange() { }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }
    
    public Integer getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(Integer multiplier) {
        this.multiplier = multiplier;
    }
    
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "CurrencyExchange{" +
                "id=" + id +
                ", currency='" + currency + '\'' +
                ", rate=" + rate +
                ", multiplier=" + multiplier +
                ", date=" + date +
                '}';
    }
}
