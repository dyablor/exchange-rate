package com.robert.exchangerate.service;

import com.robert.exchangerate.data.CurrencyExchange;
import com.robert.exchangerate.data.CurrencyExchangeOperations;
import com.robert.exchangerate.data.CurrencyExchangeInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.List;


@Service
public class CurrencyExchangeService {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private CurrencyExchangeOperations currencyExchangeOperations;

    @Transactional
    public void updateAllRates(List<CurrencyExchange> rates) {
        log.debug("Deleting rates");
        currencyExchangeOperations.findAll().forEach(rate -> currencyExchangeOperations.delete(rate));
        entityManager.flush();

        log.debug("Adding rates");
        
        try {
            rates.forEach(rate -> entityManager.persist(rate));
        } catch (NullPointerException npe){
            log.error("Could not update rates.", npe);
        }
    }

    public List<CurrencyExchange> getAllRates() {
        return currencyExchangeOperations.findAll();
    }

    public CurrencyExchange getCurrencyDetails(String currency) {
        return currencyExchangeOperations.findByCurrency(currency.toUpperCase());
    }
    
    public CurrencyExchangeInfo performExchange(String fromCurrency, String toCurrency, Double amount) {
        final DecimalFormat decimalFormat = new DecimalFormat("#.##");
        LocalDate publishingDate = getCurrencyDetails("EUR").getDate();
        double fromExchangeRateVal = 1.0;
        double toExchangeRateVal = 1.0;

        try {
            fromExchangeRateVal = getExchangeRateValue(fromCurrency, fromExchangeRateVal);
            toExchangeRateVal = getExchangeRateValue(toCurrency, toExchangeRateVal);

            double finalRate = (amount * fromExchangeRateVal) / toExchangeRateVal;
            return new CurrencyExchangeInfo(fromCurrency, finalRate, toCurrency, decimalFormat.format(finalRate), publishingDate);
        } catch (Exception e) {
            log.error("Could not convert: from [{}], to [{}], amount: [{}], {}", fromCurrency, toCurrency, amount, e);
            return null;
        }
    }
    
    private double getExchangeRateValue(String currency, double exchangeRateVal) {
        if (!"RON".equalsIgnoreCase(currency)) {
            CurrencyExchange currencyExchange = getCurrencyDetails(currency);
            Double rate = currencyExchange.getRate();
            Integer multiplier = hasMultiplier(currencyExchange) ? currencyExchange.getMultiplier() : 1;
            exchangeRateVal =  rate / multiplier;
        }

        return exchangeRateVal;
    }    
    
     private boolean hasMultiplier(CurrencyExchange currencyExchange) {
        return currencyExchange.getMultiplier() != null;
    }
    
}
