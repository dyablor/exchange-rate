package com.robert.exchangerate.service;

import com.robert.exchangerate.data.CurrencyExchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RateUpdateService {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DataRetrieverService dataRetrieverService;

    @Autowired
    private CurrencyExchangeService currencyExchangeService;

    @Scheduled(fixedRateString = "${update.rate.interval}")
    public void updateExchangeRates() {
        log.info("Updating exchange rates");
        try {
            List<CurrencyExchange> updatedRates = dataRetrieverService.getRates();
            currencyExchangeService.updateAllRates(updatedRates);
            log.info("Rates updated successfully.");
        } catch (Exception e) {
            log.error("Could not update rates.", e);
        }
    }
}
