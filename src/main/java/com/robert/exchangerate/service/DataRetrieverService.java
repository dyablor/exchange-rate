package com.robert.exchangerate.service;

import com.robert.exchangerate.data.CurrencyExchange;
import com.robert.exchangerate.data.xml.CurrencyRate;
import com.robert.exchangerate.data.xml.DataSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static com.robert.exchangerate.util.SSLUtil.turnOffSslChecking;

@Service
public class DataRetrieverService {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Value("${bnr.url}")
    private String url;
    
    private final RestTemplate restTemplate;

    public DataRetrieverService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @Async
    public List<CurrencyExchange> getRates() throws KeyManagementException, NoSuchAlgorithmException {
        log.info("Updating rates...");

        turnOffSslChecking();
        DataSet dataSet = restTemplate.getForObject(url, DataSet.class);
        return processDataSet(dataSet);
    }

    private List<CurrencyExchange> processDataSet(DataSet dataSet) {
        List<CurrencyExchange> currencyExchangeList = new ArrayList<>();
        String dateString = dataSet.getBody().getCube().getDate();
        LocalDate date = LocalDate.parse(dateString, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        dataSet.getBody().getCube().getCurrencyRates().forEach(currencyRate -> {
                CurrencyExchange currencyExchange = processCurrencies(currencyRate, date);
                currencyExchangeList.add(currencyExchange);
        });
        return currencyExchangeList;
    }

    private CurrencyExchange processCurrencies(CurrencyRate currencyRate, LocalDate date) {
        return new CurrencyExchange(currencyRate.getCurrency(), currencyRate.getRate(), currencyRate.getMultiplier(), date);
    }
}
