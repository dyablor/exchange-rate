# Project info

The exchange-rate is a Java web application written in Java, using the Spring Boot Framework to expose RESTful services which return the exchange rate for RON and other currencies provided by the National Bank of Romania. 

It can also calculate the exchange rate for given currencies on Telegram via Chat Bot.

It uses in-memory H2 database to store the exchange rates retrieved, and it refreshes every 5 minutes.

# Application development information

Development time: 18-20 hours (no prior Spring experience)

Sources: stackoverflow.com, baeldung.com, spring.io, github.com, thymeleaf.org, makeareadme.com

Stack: Spring Boot 2.3.4, Java 11

## Installation

To start the application, you have to run the ExchangeRateApplication from IntelliJ IDEA (or any other IDE of your choosing) and access the following link in your browser:

```
http://localhost:8080/
```
The default username and password are both set to: test

Optionally, you can start ExchangeRateBot and join the Telegram bot via the link
```http://t.me/robert_exchange_rate_bot```. 

After joining the Telegram bot you can type ```/help``` for available commands.

## RESTful API for Web App
This project exposes the following RESTful APIs which respond to HTTP GET requests:

##### exchange-rate/{currency}/
Returns the parity of {currency} against the default currency (RON)

Accepts a parameter "transform" that specifies the second currency to check the parity against.


##### exchange-rate/all
Returns all the exchange rate data for RON from the database

## Examples for Web App

User: test
Pass: test

#### exchange-rate/all
```
localhost:8080/exchange-rate/all
```

```JSON
[
    {"currency":"AED","rate":1.1371,"multiplier":null,"date":"2020-10-30"}
    {"currency":"MDL","rate":0.2444,"multiplier":null,"date":"2020-10-30"}
    {"currency":"XAU","rate":251.6793,"multiplier":null,"date":"2020-10-30"}
    //...
]
```
##### exchange-rate/{currency}/
```
localhost:8080/exchange-rate/eur/
```

```JSON
[
    {"firstCurrency":"eur","parity":4.8743,"secondCurrency":"RON","roundedParity":"4.87","date":"2020-10-30"}
]
```

#### exchange-rate/{currency}/?transform={anotherCurrency}

```
localhost:8080/exchange-rate/usd/?transform=eur
```

```JSON
[
    {"firstCurrency":"usd","parity":0.856902529593993,"secondCurrency":"eur","roundedParity":"0.86","date":"2020-10-30"}
]
```

## Examples for Telegram
Join the Telegram bot via the link ```http://t.me/robert_exchange_rate_bot```

Type: /exchange 10 eur ron

Sample answer:

```
The exchange rate for: 10 eur = 48.74 ron
```

## Future directions and improvements
Better test coverage 

Better error handling 

Login service and credentials storage in DB

Use a more robust authentication mechanism